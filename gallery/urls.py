from django.urls import path
from . import views
from django.conf.urls import url, include
from django.contrib import admin
from gallery import views

urlpatterns = [
    url(r'^gallery/$', views.gallery, name='gallery'),
    url(r'^gallery/lovestory/$', views.lovestory, name='lovestory'),
    url(r'^gallery/individual/$', views.individual, name='individual'),
    url(r'^gallery/family/$', views.family, name='family'),
    # url(r'^gallery/(?P<gallery_category>\w+)/$', views.galleries, name='galleries'),
    url(r'^gallery/(?P<gallery_id>\w+)/$', views.galleries, name='galleries'),

]